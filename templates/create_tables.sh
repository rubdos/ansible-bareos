#!/bin/sh
# {{ ansible_managed }}

#
# Source the Bareos config functions.
#
. /usr/lib/bareos/scripts/bareos-config-lib.sh
bareos_sql_ddl=`get_database_ddl_dir`
sql_definitions="${bareos_sql_ddl}/creates/mysql.sql"

get_translated_sql_file ${sql_definitions} > {{ temp_sql_schema }}
